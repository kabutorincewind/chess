<?php
//start
//drawBoard
//enterMove
//checkBoard
//if endgame ask -> start | exit
class ChessGame{

    protected $board;
    protected $ranks;

    protected $cannotCastle;
    protected $movedRookA;
    protected $movedRookB;
    protected $movedKing ;
    protected $score;
    protected $player;

    public function __construct()
    {
        $this->board = $this->getBoard();
        $this->ranks = ['1' => 'a', '2' => 'b', '3' => 'c', '4' => 'd', '5' => 'e', '6' => 'f', '7' => 'g', 8 => 'h'];
        $this->player = 1;
        $this->cannotCastle = [1 => false, 2 => false];
        $this->movedRookA = [1 => false, 2 => false];
        $this->movedRookB = [1 => false, 2 => false];
        $this->movedKing = [1 => false, 2 => false];
        $this->score = [];
    }
    protected function getBoard()
    {
        return [
            81 => 'R', 82 => 'N', 83 => 'B', 84 => 'Q', 85 => 'K', 86 => 'B', 87 => 'N', 88 => 'R',
            71 => 'P', 72 => 'P', 73 => 'P', 74 => 'P', 75 => 'P', 76 => 'P', 77 => 'P', 78 => 'P',
            61 => ' ', 62 => ' ', 63 => ' ', 64 => ' ', 65 => ' ', 66 => ' ', 67 => ' ', 68 => ' ',
            51 => ' ', 52 => ' ', 53 => ' ', 54 => ' ', 55 => ' ', 56 => ' ', 57 => ' ', 58 => ' ',
            41 => ' ', 42 => ' ', 43 => ' ', 44 => ' ', 45 => ' ', 46 => ' ', 47 => ' ', 48 => ' ',
            31 => ' ', 32 => ' ', 33 => ' ', 34 => ' ', 35 => ' ', 36 => ' ', 37 => ' ', 38 => ' ',
            21 => 'p', 22 => 'p', 23 => 'p', 24 => 'p', 25 => 'p', 26 => 'p', 27 => 'p', 28 => 'p',
            11 => 'r', 12 => 'n', 13 => 'b', 14 => 'q', 15 => 'k', 16 => 'b', 17 => 'n', 18 => 'r'
        ];
    }
    protected function isChecked($player, $move = NULL)
    {
        $board = $this->board;
        if($move)
        {
            $from = key($move);
            $to = $move[$from];
            // Is it a castling move ?
            if($this->cannotCastle[$player] == false && strtolower($board[$from]) == 'k' && abs($to-$from) == 2)
            {
                if($from > $to) {
                    // Long castle
                    $board[$to] = $board[$from];
                    $board[$from] = ' ';
                    $board[($to+1)] = $board[($from-4)];
                    $board[($from-4)] = ' ';
                } else {
                    // Short castle
                    $board[$to] = $board[$from];
                    $board[$from] = ' ';
                    $board[($to-1)] = $board[($from+3)];
                    $board[($from+3)] = ' ';
                }
            }
            else {
                $board[$to] = $board[$from];
                $board[$from] = ' ';
            }
        }
        $myKing = ($player == 1) ? 'k' : 'K';
        $tmpboard = $board;
        foreach($board as $sqm => $piece)
        {
            $t_piece = strtolower($piece);
            if($board[$sqm] != ' ') {
                // Check if it's our own piece
                $isOurOwn = ($player == 1) ? ((strtolower($board[$sqm]) == $board[$sqm]) ? true : false) : ((strtoupper($board[$sqm]) == $board[$sqm]) ? true : false);
                if(!$isOurOwn)
                {
                    $curPos = $sqm;
                    if($t_piece == 'p') {
                        if($player == 1) {
                            $tmpboard[$sqm-9] = '#';
                            $tmpboard[$sqm-11] = '#';
                        }
                        else {
                            $tmpboard[$sqm+9] = '#';
                            $tmpboard[$sqm+11] = '#';
                        }
                    }
                    if($t_piece == 'b' || $t_piece == 'q') {
                        $movements = Array(11,-11,9,-9);
                        foreach($movements as $movement) {
                            $curPos = $sqm;
                            $outofbounds = false;
                            while($outofbounds == false)
                            {
                                $curPos += $movement;
                                $t_file = ($curPos%10);
                                $t_rank = ($curPos - ($curPos % 10))/10;
                                if($t_rank > 8 || $t_file > 8 || $t_rank < 1 || $t_file < 1) {
                                    $outofbounds = true;
                                } else {
                                    if($board[$curPos] == ' ') {
                                        $tmpboard[$curPos] = '#';
                                    } else {
                                        if($board[$curPos] == $myKing) {
                                            $tmpboard[$curPos] = '#';
                                        }
                                        $outofbounds = true; // Blocking piece
                                    }
                                }
                            }
                        }
                    }
                    if($t_piece == 'r' || $t_piece == 'q') {
                        $movements = Array(10,-10,-1,1);
                        foreach($movements as $movement) {
                            $curPos = $sqm;
                            $outofbounds = false;
                            while($outofbounds == false)
                            {
                                $curPos += $movement;
                                $t_file = ($curPos%10);
                                $t_rank = ($curPos - ($curPos % 10))/10;
                                if($t_rank > 8 || $t_file > 8 || $t_rank < 1 || $t_file < 1) {
                                    $outofbounds = true;
                                } else {
                                    if($board[$curPos] == ' ') {
                                        $tmpboard[$curPos] = '#';
                                    } else {
                                        if($this->board[$curPos] == $myKing) {
                                            $tmpboard[$curPos] = '#';
                                        }
                                        $outofbounds = true; // Blocking piece
                                    }
                                }
                            }
                        }
                    }
                    if($t_piece == 'n') {
                        $tmpboard[($sqm+21)] = '#';
                        $tmpboard[($sqm-21)] = '#';
                        $tmpboard[($sqm+19)] = '#';
                        $tmpboard[($sqm-19)] = '#';
                        $tmpboard[($sqm-12)] = '#';
                        $tmpboard[($sqm+12)] = '#';
                        $tmpboard[($sqm-8)] = '#';
                        $tmpboard[($sqm+8)] = '#';
                    }
                    if($t_piece == 'k') {
                        $tmpboard[($sqm+1)] = '#';
                        $tmpboard[($sqm-1)] = '#';
                        $tmpboard[($sqm-10)] = '#';
                        $tmpboard[($sqm+10)] = '#';
                        $tmpboard[($sqm-9)] = '#';
                        $tmpboard[($sqm-11)] = '#';
                        $tmpboard[($sqm+9)] = '#';
                        $tmpboard[($sqm+11)] = '#';
                    }
                }
            }
        }
        $foundKing = false;
        foreach($tmpboard as $square => $piece) {
            if($piece == $myKing) {
                $foundKing = true;
            }
        }
        return $foundKing? false : true;
    }
    protected function printBoard()
    {
        $lineNums = [8,7,6,5,4,3,2,1,''];
        $i=0;
        foreach($this->board as $square => $piece)
        {
            if(!$i) {
                echo '  ---------------------------------' . PHP_EOL . ($lineNums[$i]) .' | ';
            }
            ++$i;
            echo $piece . ' | ';
            echo (!($i%8) ? PHP_EOL . '  ---------------------------------' . PHP_EOL . $lineNums[($i/8)] . ($i < 64 ? ' | ' : '') : '');
        }
        echo '  .';
        foreach ($this->ranks as $k => $v) {
            echo ' '.$v.' .';
        }
        echo PHP_EOL;
    }
    protected function getMoveList($player = 1)
    {
        $pieces = [];
        $pieces2 = [];
        $potential_moves = [];
        foreach($this->board as $square => $piece)
        {
            if($player == 1 && strtolower($piece) == $piece && $piece != ' ') {
                $pieces[$square] = $piece;
            }
            else if($player == 1 && strtolower($piece) != $piece && $piece != ' ') {
                $pieces2[$square] = $piece;
            }
            else if($player == 2 && strtoupper($piece) == $piece && $piece != ' ') {
                $pieces[$square] = $piece;
            }
            else if($player == 2 && strtoupper($piece) != $piece && $piece != ' ') {
                $pieces2[$square] = $piece;
            }
        }
        foreach ($pieces as $square => $piece)
        {
            $file = ($square%10);
            $rank = ($square - ($square % 10))/10;
            $t_piece = strtolower($piece);
            if($t_piece == 'p') {
                if($player == 1) {
                    // Moving forward
                    if($this->board[($square+10)] == ' ' && $rank < 8) {
                        $potential_moves[] = Array($square => ($square+10));
                    }
                    if ($this->board[($square+20)] == ' ' && $this->board[($square+10)] == ' ' && $rank == 2) {
                        $potential_moves[] = Array($square => ($square+20));
                    }
                    // Capturing
                    if($pieces2[($square+11)] && $file < 8) {
                        $potential_moves[] = Array($square => ($square+11));
                    }
                    if($pieces2[($square+9)] && $file > 1) {
                        $potential_moves[] = Array($square => ($square+9));
                    }
                }
                else if ($player == 2) {
                    // Moving back Player 2 moves: Ph6 to: 7
                    if($this->board[($square-10)] == ' ' && $rank > 1) {
                        $potential_moves[] = Array($square => ($square-10));
                    }
                    if ($this->board[($square-20)] == ' ' && $this->board[($square-10)] == ' ' && $rank == 7) {
                        $potential_moves[] = Array($square => ($square-20));
                    }
                    // Capturing
                    if($pieces2[($square-11)] && $file > 1) {
                        $potential_moves[] = Array($square => ($square-11));
                    }
                    if($pieces2[($square-9)] && $file < 8) {
                        $potential_moves[] = Array($square => ($square-9));
                    }
                }
            }
            else if ($t_piece == 'n') {
                // Two forward, 1 left
                if($file > 1 && $rank < 7 && !$pieces[($square+19)])
                {
                    $potential_moves[] = Array($square => ($square+19));
                }
                // Two forward, 1 right
                if($file < 8 && $rank < 7 && !$pieces[($square+21)])
                {
                    $potential_moves[] = Array($square => ($square+21));
                }
                // Two right, 1 forward
                if($file < 7 && $rank < 8 && !$pieces[($square+12)]) {
                    $potential_moves[] = Array($square => ($square+12));
                }
                // Two right, 1 back
                if($file < 7 && $rank > 1 && !$pieces[($square-8)]) {
                    $potential_moves[] = Array($square => ($square-8));
                }
                // Two down, 1 right
                if($rank > 2 && $file < 8 && !$pieces[($square-19)]) {
                    $potential_moves[] = Array($square => ($square-19));
                }
                // Two down, 1 left
                if($rank > 2 && $file > 1 && !$pieces[($square-21)]) {
                    $potential_moves[] = Array($square => ($square-21));
                }
                // Two left, 1 back
                if($file > 2 && $rank > 1 && !$pieces[($square-12)]) {
                    $potential_moves[] = Array($square => ($square-12));
                }
                // Two left, 1 forward
                if($file > 2 && $rank < 8 && !$pieces[($square+8)]) {
                    $potential_moves[] = Array($square => ($square+8));
                }
            }
            else if ($t_piece == 'b') {
                // Diagonal forward right
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm += 11;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank > 8 || $t_file > 8 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Diagonal back right
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm -= 9;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank < 1 || $t_file > 8 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Diagonal back left
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm -= 11;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank < 1 || $t_file < 1 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Diagonal forward left
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm += 9;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank > 8 || $t_file < 1 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
            }
            else if ($t_piece == 'q') {
                // Diagonal forward right
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm += 11;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank > 8 || $t_file > 8 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Diagonal back right
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm -= 9;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank < 1 || $t_file > 8 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Diagonal back left
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm -= 11;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank < 1 || $t_file < 1 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Diagonal forward left
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm += 9;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank > 8 || $t_file < 1 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Horizontal left
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm--;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_file < 1 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Horizontal right
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    ++$sqm;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_file > 8 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Vertical forward
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm += 10;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank > 8 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Vertical back
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm -= 10;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank < 1 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
            }
            else if ($t_piece == 'r') {
                // Horizontal left
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm--;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_file < 1 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Horizontal right
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    ++$sqm;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_file > 8 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Vertical forward
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm += 10;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank > 8 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                // Vertical back
                $sqm = $square;
                $hitobstacle = false;
                while($hitobstacle == false)
                {
                    $sqm -= 10;
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($t_rank < 1 || $pieces[$sqm]) {
                        $hitobstacle = true;
                    }
                    else
                    {
                        if($pieces2[($sqm)]) {
                            $potential_moves[] = Array($square => $sqm);
                            $hitobstacle = true;
                        }
                        else {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
            }
            else if ($t_piece == 'k') {
                $movements = Array($square-1,$square+1,$square+10,$square-10,$square-9,$square-11,$square+9,$square+11);
                foreach($movements as $sqm) {
                    $t_file = ($sqm%10);
                    $t_rank = ($sqm - ($sqm % 10))/10;
                    if($this->board[$sqm] == ' ' || !$pieces[$sqm]) {
                        if($t_rank >= 1 && $t_rank <= 8 && $t_file >= 1 && $t_file <= 8) {
                            $potential_moves[] = Array($square => $sqm);
                        }
                    }
                }
                if($this->cannotCastle[$player] == false && !$this->isChecked($player))
                {
                    // Castle long
                    $myRook = ($player == 1) ? 'r' : 'R';
                    if(!$this->movedRookA[$player] && $this->board[($square-1)] == ' ' && $this->board[($square-2)] == ' ' && $this->board[($square-3)] == ' ' && $this->board[($square-4)] == $myRook) {
                        $potential_moves[] = Array($square => ($square-2));
                    }
                    // Castle short
                    if(!$this->movedRookB[$player] && $this->board[($square+1)] == ' ' && $this->board[($square+2)] == ' ' && $this->board[($square+3)] == $myRook) {
                        $potential_moves[] = Array($square => ($square+2));
                    }
                }
            }
        }
        $movenumber = 0;
        foreach($potential_moves as $testMove) {
            if($this->isChecked($player, $testMove))
            {
                unset($potential_moves[$movenumber]);
            }
            ++$movenumber;
        }
        return $potential_moves;
    }
    public function startGame(){
        echo 'Game starts, anytime enter S to save, L to load, ctrl+c to exit'.PHP_EOL;
        $player = 1;
        $firstmove = '';
        while(true){
            $this->printBoard();
            echo 'Player:'.$player.PHP_EOL;
            $moves = @$this->getMoveList($player);
            if(count($moves) === 0) {
                $staleMate = ($this->isChecked($player) == true) ? false : true;
                if($staleMate) {
                    echo "Player $player bas been stalemated. 0.5-0.5\n";
                    $this->score[$firstmove]+=0.5;
                } else {
                    echo "Player $player bas been mated. " . ($player == 1 ? '0-1' : '1-0') . "\n";
                    if($player == 1) {
                        $this->score[$firstmove]--;
                    }
                    else {
                        $this->score[$firstmove] = $this->score[$firstmove] + 1;
                    }
                }
                echo 'Start new game? (yes|no) ';
                $handle = fopen ("php://stdin","r");
                $decision = fgets($handle);
                if($decision == 'yes'){
                    $this->startGame();
                }else{
                    return;
                }
            }
            echo 'Possible moves:'.PHP_EOL;
            foreach ($moves as $n => $move) {
                foreach ($move as $start => $end) {
                    $s = (string) $start;
                    $e = (string) $end;
                    echo $this->ranks[$s[1]].$s[0].' > '.$this->ranks[$e[1]].$e[0];
                    echo ' | ';
                }
            }
            list($handle, $move) = $this->inputMove();
            if(count($move) == 1){
                $tomove = $moves[0];
            }else{
                $m_key = (int) $move[1].array_search($move[0], $this->ranks);
                $m_value = (int) $move[3].array_search($move[2], $this->ranks);
                $tomove_key = array_search([ $m_key => $m_value], $moves);
                $tomove = $moves[(integer) $tomove_key];
            }
            $sqm = key($tomove);
            $sqm2 = $tomove[$sqm];
            $t_file = ($sqm%10);
            $t_rank = ($sqm - ($sqm % 10))/10;
            $t_file2 = ($sqm2 % 10);
            $t_rank2 = ($sqm2 - ($sqm2 % 10))/10;
            if(strtolower($this->board[$sqm2]) != 'k') {
                // Castle
                if(strtolower($this->board[$sqm]) == 'k' && abs($sqm-$sqm2) == 2)
                {
                    $cannotCastle[$player] = true;
                    if($sqm > $sqm2) {
                        // Long castle
                        $this->board[$sqm2] = $this->board[$sqm];
                        $this->board[$sqm] = ' ';
                        $this->board[($sqm2+1)] = $this->board[($sqm-4)];
                        $this->board[($sqm-4)] = ' ';
                        if(!$firstmove)
                            $firstmove = 'O-O-O';
                        echo 'Player '.$player.' moves: O-O-O' . PHP_EOL;
                    } else {
                        // Short castle
                        $this->board[$sqm2] = $this->board[$sqm];
                        $this->board[$sqm] = ' ';
                        $this->board[($sqm2-1)] = $this->board[($sqm+3)];
                        $this->board[($sqm+3)] = ' ';
                        if(!$firstmove)
                            $firstmove = 'O-O';
                        echo 'Player '.$player.' moves: O-O' . PHP_EOL;
                    }
                }
                else {
                    // Detect rook and king moves
                    if(strtolower($this->board[$sqm]) == 'k') {
                        $this->movedKing[$player] = true;
                        $this->cannotCastle[$player] = true;
                    }
                    if(strtolower($this->board[$sqm]) == 'r' && $t_file == 1)
                    {
                        $this->movedRookA[$player] = true;
                    }
                    if(strtolower($this->board[$sqm]) == 'r' && $t_file == 8)
                    {
                        $this->movedRookB[$player] = true;
                    }
                    if($this->movedRookB[$player] && $this->movedRookA[$player] == true) {
                        $this->cannotCastle[$player] = true;
                    }
                    if(strtolower($this->board[$sqm]) == 'p' && ($t_rank2 == 1 || $t_rank2 == 8)) {
                        // Pawn promotion
                        shuffle($promotion);
                        $promote_to = ($player == 1) ? array_pop($promotion) : strtoupper(array_pop($promotion));
                        $this->board[$sqm2] = $promote_to;
                        $this->board[$sqm] = ' ';
                        if(!$firstmove)
                            $firstmove = $this->board[$sqm] . $this->ranks[$t_file] . $t_rank . '-' . $this->ranks[$t_file2] . $t_rank2 . '=' . strtoupper($promote_to);
                        echo 'Player '.$player.' moves: ' . $this->board[$sqm] . $this->ranks[$t_file] . $t_rank . ' to: ' . $this->ranks[$t_file2] . $t_rank2 . ' and promotes to ' . strtoupper($promote_to) . PHP_EOL;
                    }
                    else {
                        if(!$firstmove)
                            $firstmove = $this->board[$sqm] . $this->ranks[$t_file] . $t_rank . '-' . $this->ranks[$t_file2] . $t_rank2;
                        echo 'Player '.$player.' moves: ' . $this->board[$sqm] . $this->ranks[$t_file] . $t_rank . ' to: ' . $this->ranks[$t_file2] . $t_rank2 . PHP_EOL;
                        $this->board[$sqm2] = $this->board[$sqm];
                        $this->board[$sqm] = ' ';
                    }
                }
                $player = ($player == 1) ? 2 : 1;
            }
        }
    }
    public function saveGame($comment){
        $sqlite = new SQLite3('save.sqlite');
        $sqlite->query('create table if not exists save (id integer primary key autoincrement, serialization text, comment varchar);');
        $serialized = addslashes(serialize($this));
        $statement = $sqlite->prepare('insert into save(serialization, comment) values (:serialization, :comment)');
        $statement->bindValue(':serialization', $serialized, SQLITE3_TEXT);
        $statement->bindParam(':comment', $comment, SQLITE3_TEXT);
        return $statement->execute();
    }
    public function menu(){
        echo 'Enter N to start new game, L to load: ';
        $handle = fopen ("php://stdin","r");
        $command = trim(fgets($handle));
        if($command == 'N'){
            $this->startGame();
        }elseif($command == 'L'){
            $this->loadMenu();
        }
    }
    public function listSavedGames()
    {
        $sqlite = new SQLite3('save.sqlite');
        try{
            $statement = @$sqlite->prepare('select * from save;');
            $savedGames = $statement->execute();
        }catch(\Throwable $e){
            echo 'No saved games exists'.PHP_EOL;
            $this->menu();
            return;
        }
        do {
            $result = $savedGames->fetchArray();
            echo $result['id'].':'.$result['comment'].PHP_EOL;
        } while ($result);
    }
    public function loadGame($id)
    {
        $sqlite = new SQLite3('save.sqlite');
        $statement = $sqlite->prepare('select * from save where id = :id');
        $statement->bindParam(':id', $id);
        $result = $statement->execute()->fetchArray();
        if(!$result) return false;
        $save = unserialize(stripslashes($result['serialization']));
        $this->board = $save->board;
        $this->ranks = $save->ranks;
        $this->cannotCastle = $save->cannotCastle;
        $this->movedRookA = $save->movedRookA;
        $this->movedRookB = $save->movedRookB;
        $this->movedKing = $save->movedKing;
        $this->score = $save->score;
        $this->startGame();
    }
    protected function loadMenu()
    {
        $this->listSavedGames();
        echo 'Enter saved game id or Q to return:';
        $handle = fopen("php://stdin", "r");
        $id = fgets($handle);
        if ($id == 'Q') {
            $this->menu();
        } else {
            $id = (int)$id;
            $this->loadGame($id);
        }
    }

    /**
     * @return resource
     */
    protected function saveMenu()
    {
        echo PHP_EOL . 'Enter comment:';
        $handle = fopen("php://stdin", "r");
        $comment = fgets($handle);
        $saved = $this->saveGame($comment);
        echo 'Game saved.'.PHP_EOL;
        return $saved;
    }

    /**
     * @return array
     */
    protected function inputMove()
    {
        echo PHP_EOL . 'Enter move:';
        $handle = fopen("php://stdin", "r");
        $move = trim(fgets($handle));
        if ($move == 'S') {
            $this->saveMenu();
            $this->inputMove();
        } elseif ($move == 'L') {
            $this->loadMenu();
            $this->startGame();
        }
        return array($handle, $move);
    }
}
(new ChessGame())->menu();